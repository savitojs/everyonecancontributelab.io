---
Title: "Join"
Date: 2021-04-15
Authors: ["dnsmichi"]
mermaid: true
---

Please [join Discord](/page/handbook/#discord) for questions.

## Join our coffee chats

- Live on YouTube: [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A). [Join Discord](/page/handbook/#discord) for chats and insights.
- Live in Zoom: Please join below.

The recordings are available in these playlists:

- [Cafe (English)](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp1Gni9SyudMmXmBJIp7rIc)
- [Kaeffchen (German)](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI)

### Join the English cafe in Zoom

Please add your contact details to [this form](https://forms.gle/1Km1emjpZvoUGTQh8). Michael invites you to the Zoom session and GitLab project.

<!--
### Join the German Kaeffchen in Zoom

Please add your contact details to [this form](https://forms.gle/LiK1ANVbfNSt89hW9). Michael invites you to the Zoom session and GitLab project.

-->
