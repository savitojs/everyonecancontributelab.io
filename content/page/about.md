---
Title: "About"
Date: 2020-12-07
Authors: ["dnsmichi"]
mermaid: true
---

Technology is moving fast, and often we want to try things but lack the time to. Best practices and learnings need discussion, or a live debugging session in a cosy coffee chat.

* [Join us](/page/join/)
* [Who we are](#who-we-are)

This idea formed the `#everyonecancontribute` coffee chat, one hour with a topic or theme. This can be a new project announced earlier, an introduction into a new OSS startup or learning a new technology or programming language together. This way, we learned about [Terraform](/post/2020-07-01-kaeffchen-7/), [Conan for C++](/post/2020-07-15-kaeffchen-9/), [Vector, Loki and Grafana](/post/2020-07-29-kaeffchen-11/), [Docker Hub rate limits](/post/2020-09-02-kaeffchen-17/), [GitLab CI rules](/post/2020-09-23-kaeffchen-19/), [QuestDB](/post/2020-09-23-cafe-1/), [Gitpod](/post/2020-09-30-kaeffchen-20-gitpod/), [CI/CD Security with Vault](/post/2020-09-30-cafe-2-vault-ci/), [first steps with Rust in Gitpod](/post/2020-10-07-cafe-3-gitpod-gitlab-rust/), [Jina.ai, an OSS neural search framework](/post/2020-10-14-cafe-4-jina-ai/), [HashiCorp Waypoint](/post/2020-10-21-cafe-5-hashicorp-waypoint/), [Grafana Tempo](/post/2020-10-28-cafe-6-grafana-tempo/), [Zero trust](/post/2020-10-28-kaeffchen-23-zero-trust/), [Docker Hub rate limit mitigation](/post/2020-11-04-cafe-7-docker-hub-rate-limit-monitoring/), [Puppet 7](/post/2020-11-04-kaeffchen-24-puppet-insights/), [Keptn, a control pane for CD in cloud-native apps](/post/2020-11-11-cafe-8-keptn/), [AWS re:invent macOS EC2 & Proton](/post/2020-12-02-cafe-9-aws-reinvent-macos-proton/) and so much more!

The [first iteration](/post/2020-05-28-first-post/) was German, with more interest adding a second English iteration. Join us every Wednesday (except holidays) at

- 6pm CET / 9am PT English cafe

The German Kaeffchen is paused until afternoon availability is better.

The sessions are hosted by [Michael Friedrich, Developer Evangelist at GitLab](/post/2020-05-28-first-post/) in their Zoom and calendar.

Check our [Handbook](/page/handbook/) for more insights.


## Who we are

Name                  | <i class="fab fa-twitter"></i> Twitter | <i class="fab fa-linkedin"></i> LinkedIn | <i class="fab fa-gitlab"></i> GitLab
----------------------|-----------------------|------------------------|-------
Michael Friedrich     | [@dnsmichi](https://twitter.com/dnsmichi) | [Michael Friedrich](https://www.linkedin.com/in/dnsmichi/) | [dnsmichi](https://gitlab.com)
Nico Meisenzahl       | [@nmeisenzahl](https://twitter.com/nmeisenzahl) | [Nico Meisenzahl](https://www.linkedin.com/in/nicomeisenzahl/) | [nico-meisenzahl](https://gitlab.com/nico-meisenzahl)
Niclas Mietz          | [@solidnerd](https://twitter.com/solidnerd) | [Niclas Mietz](https://www.linkedin.com/in/niclas-mietz-794053115/) | [Niclas Mietz](https://gitlab.com/solidnerd)
Michael Aigner        | [@tonka3000](https://twitter.com/@tonka_2000) | [Michael Aigner](https://www.linkedin.com/in/michael-aigner-ab06809/) | [tonka3000](https://gitlab.com/tonka3000)
David Schmitt         | [@dev_el_ops](https://twitter.com/dev_el_ops) | [David Schmitt](https://www.linkedin.com/in/davidschmitt/) | [DavidS](https://gitlab.com/DavidS)
Feu Mourek            | [@the_FeuFeu](https://twitter.com/the_feufeu) | [Feu Mourek](https://www.linkedin.com/in/feu-mourek/) | [theFeu](https://gitlab.com/theFeu)
Philipp Westphalen    | [@Koala_Phil](https://twitter.com/Koala_Phil) | [Philipp Westphalen](https://www.linkedin.com/in/philipp-westphalen-a83318188/) | [Phil404](https://gitlab.com/Phil404)
Marcel Weinberg       | [@winem_](https://twitter.com/@winem_) | [Marcel Weinberg](https://www.linkedin.com/in/marcel-weinberg-a86464101/) | [winem](https://gitlab.com/winem)
Nicolai Buchwitz      | [@nicolaibuchwitz](https://twitter.com/nicolaibuchwitz) | [Nicolai Buchwitz](https://www.linkedin.com/in/nicolai-buchwitz-06174680/) | [nbuchwitz](https://gitlab.com/nbuchwitz)
Mario Kleinsasser     | [@m4r10k](https://twitter.com/m4r10k) | [Mario Kleinsasser](https://www.linkedin.com/in/mario-kleinsasser/) | [m4r10k](https://gitlab.com/m4r10k)
Bernhard Rausch       | [@rauschbit](https://twitter.com/rauschbit) | [Bernhard Rausch](https://www.linkedin.com/in/bernhard-rausch/) | [rauschbit](https://gitlab.com/rauschbit)
Markus Fischbacher    | [@rockaut](https://twitter.com/@RealRockaut) | - | [rockaut](https://gitlab.com/rockaut)
Carsten Köbke         | [@mikeschova](https://twitter.com/mikeschova) | [Carsten Köbke](https://www.linkedin.com/in/carsten-koebke-a721ab161/) |    [Mikeschova](https://gitlab.com/Mikeschova)
Moritz Tanzer         | [@moritztanzer](https://twitter.com/moritztanzer) | [Moritz Tanzer](https://www.linkedin.com/in/moritz-tanzer-74292a141/) | [tanzerm](https://gitlab.com/tanzerm)
Tim Meusel            | [@BastelsBlog](https://twitter.com/BastelsBlog) | - | -
Philip Welz           | [@philip_welz](https://twitter.com/philip_welz) | [Philip Welz](https://www.linkedin.com/in/philip-welz/) | [phil.xx](https://gitlab.com/phil.xx)





