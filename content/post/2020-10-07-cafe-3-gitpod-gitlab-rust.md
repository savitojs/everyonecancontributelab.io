---
Title: "3. Cafe: Learning Rust with Gitpod and GitLab Workshop"
Date: 2020-10-07
Aliases: []
Tags: ["gitlab","gitpod","rust","development","ide","learn"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

Following our last week's [Gitpod adventure](https://everyonecancontribute.com/post/2020-09-30-kaeffchen-20-gitpod/), I decided to create a new light-weight workshop around Gitpod and GitLab. Once settled in with an example project in Java, we took course into learning Rust from scratch.

![Gitpod IDE, GitLab and Rust](/post/images/gitpod_gitlab_learning_rust.png)

During the extended session - we wanted to fix the module import no matter the time - it became clear that Rust has many good things we know from Python (import modules and aliases), C++, Golang, and more. 

We only touched the surface thus far with covering values, types, conditions, functions, modules - and some more string magic. There is a whole lot more to unpack, especially considered the GitLab CI/CD side in our next sessions! 

Start your Gitpod journey by following the **[Workshop Slides](https://docs.google.com/presentation/d/1t1FdHh04TAOg9WITqRFJHz1YFxMbsQeekN8th1UfFcI/edit?usp=sharing)** :-)

### Recording

Enjoy the session and let us know how your Rust journey with Gitpod goes! 🦊 

{{< youtube ewDAOLTto-A >}}


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/46)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd)
- [Next ☕ chat `#4`](https://gitlab.com/everyonecancontribute/general/-/issues/48) - Jina.ai 

#### Bookmarks 

- [Rust by Example Tutorial](https://doc.rust-lang.org/rust-by-example/hello.html)
- [Rust in Gitpod](https://www.gitpod.io/docs/languages/rust/ )
- [Learn Rust - Michael's repository](https://gitlab.com/dnsmichi/gitpod-learn-rust)
- [Java Pet in Gitpod - changes](https://gitlab.com/dnsmichi/gitpod-learn-java/-/commit/52bfc2b574323e03e460ac70b47b230c07c6583f)
- [Enter the Vault: Authentication Issues in HashiCorp Vault](https://googleprojectzero.blogspot.com/2020/10/enter-the-vault-auth-issues-hashicorp-vault.html)
- [Site Reliability Engineering and how it refers to the DevOps approach](https://medium.com/just-another-buzzword/site-reliability-engineering-and-how-it-refers-to-the-devops-approach-5f641b103de7)
- [The Production Readiness Spectrum](https://dastergon.gr/posts/2020/09/the-production-readiness-spectrum/)
- [Book recommendations](https://twitter.com/dnsmichi/status/1313540191295098882)
  - [Implementing Service Level Objects](https://www.oreilly.com/library/view/implementing-service-level/9781492076803/)
  - [Distributed Tracing in Practice](https://www.oreilly.com/library/view/distributed-tracing-in/9781492056621/)
  - [Monolith to Microservices](https://www.oreilly.com/library/view/monolith-to-microservices/9781492047834/)
  - [Help your kids with computer science](https://www.dk.com/us/book/9780241302293-help-your-kids-with-computer-science/) 


