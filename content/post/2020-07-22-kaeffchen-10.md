---
Title: "10. Kaeffchen: From Ansible, Puppet and Zabbix to Log Processing with Loki, Vector and Elastic"
Date: 2020-07-22
Aliases: []
Tags: ["gitlab","devsecops","monitoring","infrastructureascode"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/20)
- Guests: Bernhard Rausch, Claudio Künzler, Markus Koller, Niclas Mietz, Nico Meisenzahl, Michael Friedrich
- Next ☕ chat `#11`: **2020-07-29** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/22)

### Highlights

- Welcome [Claudio Künzler](https://gitlab.com/ckuenzler) and [Markus Koller](https://gitlab.com/toupeira)!
- [Provisioning VMware Ubuntu 20.04 LTS images with Ansible](https://github.com/vmware/open-vm-tools/issues/421#issuecomment-619276881)
- Puppet, Ansible, Zabbix use cases - Bolt?
- [Network Inventory Automation - Twitter thread](https://twitter.com/xer0beat/status/1284738106390765570)
- [Monitoring insights from OSMC 2019 - YT playlist](https://www.youtube.com/watch?v=3337-ZHDUYM&list=PLeoxx10paaAktrDeftMa6KT0_ps02U6qh)
- Grafana Loki - event & metric format & [clients](https://grafana.com/docs/loki/latest/clients/) (promtail, fluentd)
- [Vector](https://github.com/timberio/vector), fast log processing in Rust
- [Logstash or Elastic ingest nodes](https://www.elastic.co/blog/introducing-the-enrich-processor-for-elasticsearch-ingest-nodes) 
- [Elastic Cloud Enterprise on Premise](https://www.elastic.co/ece)
- [Elastic Operator on Kubernetes](https://www.elastic.co/blog/introducing-elastic-cloud-on-kubernetes-the-elasticsearch-operator-and-beyond)
- [Elastic APM](https://www.elastic.co/apm)

### Recording

Enjoy the session!

{{< youtube E5RPrHAsCoY >}}


### News

- GitLab 13.2 Release day, yay!
  - [Release post](https://about.gitlab.com/releases/2020/07/22/gitlab-13-2-released/)
  - [Michael's Release Evangelism](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3096)
  - [Web IDE real-time feedback for CI config](https://twitter.com/dnsmichi/status/1285601938311188480) :100:
  - [Release automation from CI config](https://twitter.com/dnsmichi/status/1285279314297200642) :heart: 
- [Installing macOS Big Sur Beta on VMWare Fusion](http://www.edenwaith.com/blog/index.php?p=118)
- [Auto-updating Kernels](https://twitter.com/mattiasgeniar/status/1285839144346963968)
- [Ansible Rescue Blocks](https://twitter.com/jpmens/status/1285849898664034304)
- [Go Draft Designs](https://twitter.com/_rsc/status/1285597040815345664)
- [Cloudflare DNS - round robin solution with CoreDNS](https://twitter.com/mholt6/status/1284250606673080321)
- [C++ Build Bottleneck detection](https://twitter.com/KevinCadieuxMS/status/1285598980186021888)
- :rocket: [Systems Performance - new book](https://twitter.com/dnsmichi/status/1284024451890372608)
- [Rate limiting in Go](https://twitter.com/sethvargo/status/1283037281620819969)
- [PromQL Cheatsheet](https://twitter.com/peterbourgon/status/1281612292275281921)
- :rainbow: [Growing your blog - new book by Emma Bostian](https://twitter.com/dnsmichi/status/1283795047268442115)
