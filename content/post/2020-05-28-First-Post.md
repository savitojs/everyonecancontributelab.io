---
Title: "Everyone Can Contribute"
Subtitle: "... you too!"
Date: 2020-05-28
Description: ""
Aliases: []
Tags: []
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
Twitter:
  card: "summary"
  title: ""
  description: ""
  image: ""

---

Welcome to our lovely and cosy #everyonecancontribute page!

We've found each other in a German coffee chat for talking all things tech, best practices and social. 

### The name has a story

Gitlab [says](https://about.gitlab.com/company/):

> GitLab believes in a world where everyone can contribute. Our mission is to change all creative work from read-only to read-write. When everyone can contribute, consumers become contributors and we greatly increase the rate of human progress.
>
> With GitLab, everyone can contribute.

When Michael applied for the Developer Evangelist role at GitLab in November 2019, he had registered everyonecancontribute.com with a funny animated demo website for this interview panel presentation.

Following up on tech chats after onboarding, Michael iterated on the name:

- Keep it short: "TE Coffee Chat"
- Nobody knows what TE is: "Technical Evangelism Coffee Chat"
- Let's use the website in the name: "Everyone Can Contribute Coffee Chat"
- German, use a term: "Everyone Can Contribute Käffchen"
- Maybe use a hashtag in the name, easier for social: "#everyonecancontribute Käffchen"
- Avoid the German umlaut: "#everyonecancontribute Kaeffchen"

A [coffee chat at GitLab](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) is a short speedy meeting without the requirement for a long agenda. Since there might be quite a few topics or areas being covered by everyone, it is set to 50-60 minutes. The meeting minutes are updated on the go.

The chats are live streamed when possible, and uploaded to the GitLab Unfiltered YouTube channel into [this playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Ko8J8V5V794CXZUZ-DLxccI).

### Who we are

- [Michael Friedrich](https://gitlab.com/dnsmichi)
- [Nico Meisenzahl](https://gitlab.com/nico-meisenzahl)
- [Carsten Köbke](https://gitlab.com/Mikeschova)
- [Moritz Tanzer](https://gitlab.com/tanzerm)
- [Nicolai Buchwitz](https://gitlab.com/nbuchwitz)
- [Markus Fischbacher](https://gitlab.com/rockaut)
- [Mario Kleinsasser](https://gitlab.com/m4r10k)
- [Philipp Westphalen](https://gitlab.com/Phil404)
- [Michael Aigner](https://gitlab.com/tonka3000)
- [Marcel Weinberg](https://gitlab.com/winem)
- [Niclas Mietz](https://gitlab.com/solidnerd)
- [Bernhard Rausch](https://gitlab.com/rauschbit)


### What's going to happen in the future?

This new website is built thanks to Mario's daughter [Johanna](https://gitlab.com/chaosjolo). It uses the power of GitLab pages and adds Hugo as a framework.

The coming months will do short iterations on:

- Blog posts on everything we want to share. Languages are mixed, and tags will highlight/allow to filter.
- More website content and other ideas
- Code and demos
- Event organization and community building

The project is organized at [https://gitlab.com/everyonecancontribute](https://gitlab.com/everyonecancontribute).

See you around!
