---
Title: "Deploy VuePress with GitLab Pages"
Date: 2020-10-22
Aliases: []
Tags: ["gitlab","pages","vue","vuepress"]
Categories: ["Community"]
Authors: ["phil404"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Sometimes you need a cool looking and searchable documentation.
This is where [VuePress](https://vuepress.vuejs.org/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) comes in. 
There you can create your own documentation in lovely markdown files and host it for free.
And with this guide you can setup it in under 15 minutes.

### What is VuePress?

VuePress is a light weight static site generator made with Vue.
It suits perfectly for documentation and provides a basic search (only headlines).
Simplicity is one of the strenghts of this generator.
Even a non-technical person can create and edit the documentations.

### Install VuePress

To install VuePress you need [Node](https://nodejs.org/en/) and [NPM](https://www.npmjs.com/) on your machine.
Just execute `npx create-vuepress-site` in your git repository.
Fill out the questions if you want.
Afterwards VuePress is installed in `/docs`.
I would recommend to move up the files into your root folder of your repository (only when it's a repository only for the documentation).
In the `src` folder you can create all the documentation files and structure.
The VuePress application is located in `src/.vuepress`, where you can change everything you want.

### Configure VuePress for GitLab Pages

To use VuePress with GitLab Pages we need to do some configuration.
When you host on GitLab Pages the first path part is your project name.
But VuePress assume that you host it directly on a domain without additional pathing.
To configure the `basepath` (the standard variable name in Vue) you have to use `base` instead because this is modified by VuePress. 

Add following code snippet the file `src/.vuepress/config.js`:
```javascript
module.exports = {
  [...]
  base: process.env.CI_COMMIT_BRANCH === 'master'
    ? '/' + process.env.CI_PROJECT_NAME + '/'
    : '/'
  [...]
}
```

This snippet uses the [environment variables in the GitLab Runner](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
We use the predefined variable `CI_PROJECT_NAME` to put it in the path.
This happens when the pipeline is executed in the `master` branch, which is accessable by the variable `CI_COMMIT_BRANCH`.

### Deploy VuePress

To deploy the static site we can use one job in the deploy stage.
The `stage: deploy` is important, because it is necessary to deploy pages.
Also the artifact path is related to this, because `public` is the additional trigger for it.
Because VuePress is a javascript project the `image: node:latest` is required. 
If you use it for your case it is a good idea to use a fixed version instead of `latest`.

> Note: The split in the different script executions is for better understanding.

Before we can generate we need to install the dependencies with `npm install`.
Afterwards execute `npm run build` which generate the final static files of VuePress.
In the end we create an folder `public` which contains all the static files.
The output of the generation is stored in `src/.vuepress/dist` by default.
These files will be copied by `cp -r src/.vuepress/dist/* public` into the `public` folder.

Finally our gitlab-ci.yml is ready:
```yaml
image: node:latest

pages:
  stage: deploy
  before_script:
    - npm install
  script:
    - npm run build 
  after_script:
    - mkdir public
    - cp -r src/.vuepress/dist/* public
  artifacts:
    paths:
      - public
  only:
    - master
```

The final result can be visible under the domain: `<GitLabUser>.gitlab.io/<ProjectName>/`  
See the source code here: https://gitlab.com/Phil404/vue-press-and-gitlab-pages
